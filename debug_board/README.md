# Fixture per debug e produzione  #
In questa cartella sono presenti tutti i file di progettazione dei vari componenti per la costruzione della fixture di debug.  In particolare:

* Circuit: questa cartella contiene gli schematici e il layout per la board
* Fixture: questa cartella contiene i disegni 3D del blocco di MDF e dell'holder

## Circuit ##
***

#### Requisiti ####
* [EagleCAD v8.3.2](https://www.autodesk.com/products/eagle/overview)

### Descrizione ###
La board utilizza dei pogo-pins per connettersi ai test point della board ProtoB.  
Le interfacce di interesse che sono state connesse alla board di debug sono:

* Ethernet
* Seriale
* MicroSD
* Switch di boot **(Aperto = MicroSD, Chiuso = SOM)**
* Audio HP Codec

### Schematico ###

![schematic](../wiki/schematic.png)

### Layout ###

![layout](../wiki/layout.png)

### Bill of Material ###

ID | Valore | Descrizione | Codice Produttore
--- | --- | --- | ---
U1 | - | RJ45 | [Molex 48025-4000](https://www.mouser.it/ProductDetail/Molex/48025-4000)
U2 | - | uSD | [Molex 104031-0811](https://www.mouser.it/ProductDetail/Molex/104031-0811)
U3 | - | Jack | [CUI SJ-43514](https://www.mouser.it/ProductDetail/CUI/SJ-43514)
R1 | 22R | - | -
R2,R3 | 510R | - | -
C1 | 1uF | - | -
C2,C3,C4,C5,C6 | 100nF | - | -
Pogopins | - | Sonde a molla | [P50-B1](https://www.cariatielettronica.eu/sonde-probe-test/3289-5-pezzi-sonda-test-a-molla-pcb-ic-p50-b1-di-diametro-068-mm-spring-probe-pin.html)


## Fixture ##
***

#### Requisiti ####
* [SketchUp Free](https://www.sketchup.com/)
* [Rhinoceros v5.4.2](https://www.rhino3d.com/)
* [Vectric Aspire v8.5](https://www.vectric.com/products/aspire.html)

### Descrizione ###
La fixture è composta da un monoblocco di MDF fresato in modo da alloggiare sulla sua sommità la board, permettendo il contatto con i pogo pins sottostanti.  
I fori per i pogo pins sono di **0,70 mm**, i fori per le colonnine sono di **3,10 mm**. Gli ulteriori 2 fori di allineamento (non indispensabili) sono di **1,80 mm**. 

L'holder viene utilizzato per tenere in posizione la board sulla fixture. È un monoblocco stampato in 3D. Entrambi i componenti sono renderizzati nell'immagine seguente.

![layout](../wiki/model.png)

Nella cartella _fixture_ è stato inserito anche il progetto CAM per Vectric Aspire, utile per generare il G-Code per le lavorazioni meccaniche sul blocco di MDF.