# Deploy procedure for production  #

In questa guida sono illustrati i comandi e i passaggi da eseguire per fare il deploy del sistema sulle board RCS ProtoB, suddiviso nelle diverse procedure qui elencate:

* [Preparazione della MicroSD](#markdown-header-preparazione-della-microsd)
* [Procedura per programmare il firmware del Power Management](#markdown-header-procedura-per-programmare-il-firmware-del-power-management)
* [Procedura programmazione SOM](#markdown-header-procedura-programmazione-som)
* [Procedura di collaudo e fine linea](#markdown-header-procedura-di-collaudo-e-fine-linea)

## Preparazione della MicroSD ##
***

#### Requisiti ####
* Sistema operativo: [Ubuntu 16.04 LTS](http://releases.ubuntu.com/16.04/ubuntu-16.04.5-desktop-amd64.iso)
* HW Tools: 
    - Lettore di microSD
	- SD da almeno 8gb
* SW Tools:  
	- tar
	- bzip2
	- rename
	- pv
	
Per installare i tool richiesti, su una versione minimale di Ubuntu, lanciare i seguenti comandi:
```
$: sudo apt-get update 
$: sudo apt-get install tar bzip2 rename pv
```
	
Preparare la cartella per l'estrazione, scompattare il file di immagine e aggiungere i permessi di esecuzione dello script con i seguenti comandi:

```console
$: mkdir RCS_DEPLOY
$: cd RCS_DEPLOY
$: wget https://bitbucket.org/mobilesolutionsismb/rcs-deploy/raw/HEAD/full_image.tar.bz2 
$: tar -xjvf full_image.tar.bz2
$: chmod +x sources/meta-ismb-rcs/scripts/create_ext_sd.sh
```
	
Ogni qualvolta si voglia creare una nuova SD, eseguire i seguenti comandi da terminale dopo aver inserito la microSD nel lettore

```console
$: sudo MACHINE=imx6ul-var-dart sources/meta-ismb-rcs/scripts/create_ext_sd.sh /dev/sdX
```
**ATTENZIONE: la X in /dev/sdX deve essere sostituita con la lettera che indica il lettore di microSD (ad esempio /dev/sdb o /dev/sdc).** Per determinare l'identificativo della microSD si può eseguire il comando  
**ls -l /dev/sd\***  (oppure **ll /dev/sd\*** ) prima e dopo aver inserito la microSD nel lettore e guardare la differenza. Le righe aggiuntive determinano l'identificativo.

![ll_sd](wiki/ll_sd.png)

## Procedura per programmare il firmware del Power Management ##
***

#### Requisiti ####
* Sistema operativo: Windows
* HW Tools:	
    - programmatore [ATMEL-ICE](https://it.farnell.com/microchip/atatmel-ice/debugger-atmel-arm-avr-full-kit/dp/2407173)
* SW Tools:
	- [Atmel Studio 7](http://studio.download.atmel.com/7.0.1931/as-installer-7.0.1931-full.exe)

## **ATTENZIONE!! DURANTE LA FASE DI PROGRAMMAZIONE DEL CONTROLLER, IL SOM NON DEVE ESSERE INSERITO NELLA SCHEDA!**

	
Scaricare il file binario del firmware dal seguente link: [ControllerAlimentazione.elf](https://bitbucket.org/mobilesolutionsismb/rcs-deploy/raw/HEAD/ControllerAlimentazione.3.0.elf)  
Aprire Atmel Studio 7 in modalità amministratore.  
Aprire il tool di programmazione dal menù a tendina **Tools->Device Programming** come mostrato in figura. 

![Device Programming](wiki/device_programming.png)

Selezionare i seguenti valori dai menu a tendina, come mostrato in figura, e premere **Apply** :
  
* Tool: Atmel-ICE
* Device: ATtiny841
* Interface: ISP

![Prog Interface](wiki/prog_interface.png)

Dopo aver collegato il connettore di programmazione come mostrato in figura e alimentato la board, cliccare su **Read**, a fianco del campo **Device Signature**.  
Se tutto è collegato correttamente, nella casella di testo comparirà il codice **0x1E9315**, e la tensione rilevata sarà 3.3V.

![Prog Connection](wiki/prog_connection.jpg)

Dal menù di sinistra selezionare la voce **Production file**, e scegliere il file ELF del firmware precedentemente scaricato.  
Mettere le spunte sulle voci come mostrato in figura, e cliccare su **Program**

![Prog Settings](wiki/prog_settings.png)

Al termine della procedura, se la scrittura e la verifica sono andate a buon fine, comparirà un log riassuntivo delle operazioni eseguite.

![Prog Ok](wiki/prog_ok.png)

## Procedura programmazione SOM ##
***

#### Requisiti ####
* HW Tools:	
	- debugger di programmazione con pogo-pins
	- cavo seriale (tipo FTDI o simili)
* SW Tools:
	- terminale seriale XTerm (putty, mobaxterm, YAT...)
	
Impostare i solder jumper nella seguente configurazione:

```console
+--------+--------+
| Jumper |  Stato |
+========+========+
|   JP1  | Aperto |
+--------+--------+
|   JP2  | Aperto |
+--------+--------+
|   JP3  | Chiuso |
+--------+--------+
```	
	
Inserire la microSD precedentemente programmata (vedi sezione [Preparazione della MicroSD](#markdown-header-preparazione-della-microsd)), e lasciare il jumper sul debugger aperto.  
Collegare il cavo seriale alla porta seriale del debugger e, una volta aperto il terminale seriale sulla porta corretta, alimentare la board (12V). Sul terminale comparirà il log della procedura di avvio. 

**ATTENZIONE:** annotare la variante del SOM (**i.MX6UL** oppure **i.MX6ULL**), indicata nelle prime righe del boot

![cpu_type](wiki/cpu_type.png)

oppure lanciare il comando **model** per determinare la variante del SOM

```console
root@imx6ul-var-dart:~# model
```

Al termine di essa, inserire come login **root**, come password **p0l1f3m0** ed eseguire i seguenti comandi:
```console
root@imx6ul-var-dart:~# STOP
  
root@imx6ul-var-dart:~# install_ul				(se il tipo di variante è i.MX6UL)
oppure
root@imx6ul-var-dart:~# install_ull				(se il tipo di variante è i.MX6ULL)
```

Seguire i comandi che compaiono sullo schermo e attendere il termine della procedura automatica di flash.

![boot_ok](wiki/boot_ok.png)

In caso di errore _command not found_, correggere i permessi di esecuzione con il comando: **chmod +x /usr/bin/install_yocto.sh**

## Procedura di collaudo e fine linea ##
***
* HW Tools:
	- debugger di programmazione con pogo-pins
	- cavo seriale (tipo FTDI o simili)
	- generatore di onda sonora (altoparlante)
	- antenna GSM e GPS
	- SIM card senza pin
	- microfoni
* SW Tools:
	- terminale seriale XTerm (putty, hyperterminal, minicom, gtkterm...)
	
Rimuovere la microSD dallo slot e chiudere il jumper. Collegare il cavo seriale alla porta seriale del debugger e, una volta aperto il terminale seriale sulla porta corretta, alimentare la board (12V). Sul terminale comparirà il log della procedura di avvio.

Al termine di essa, inserire come login **root**, come password **p0l1f3m0** ed eseguire i seguenti comandi:
```console
root@imx6ul-var-dart:~# STOP
```

Il file **rcs.xml** contiene dei parametri di configurazione per la procedura di collaudo. Tale file è diviso in TASK a cui corrispondono i differenti macro componenti HW da testare (ad es. GSM, CODEC, ACCELEROMETRO, GPS, etc). Internamente alle sezioni dei task, sono presenti varie sezioni di TEST, specifici per ogni determinato TASK.

![example_conf](wiki/example_conf.png)

Per modificare i parametri di configurazione usare l'editor di testo integrato tramite il comando **nano rcs.xml**

Al termine delle modifiche, salvare il file premendo **CTRL+O** e chiudere l'editor premendo **CTRL+X**.

![nano_conf](wiki/nano_conf.png)

Infine eseguire la procedura di test con il seguente comando:

```console
root@imx6ul-var-dart:~# ./QT_EndOfLine.v.X.X.X.X rcs
```

![eol_ok](wiki/eol_ok.png)

**ATTENZIONE: le X nella riga devono essere sostituite con i numeri che indicano la versione (ad esempio 1.3.5.0). Eseguire il comando  _ls_ per verificare la versione esatta da utilizzare, oppure utilizzare l'autocompletamento tramite il tasto TAB**

Seguire le indicazioni sullo schermo.

**ATTENZIONE: durante il test di chiamata voce, non chiudere la telefonata dal cellulare, attendere la chiusura da parte del software. Se si vuole modificare il tempo di durata del test di chiamata, modificare il campo _Duration_ nella sezione _Test GSM_ **

Verificare la presenza del file *EOLReport* con il comando _ls_ e infine lanciare il seguente comando:

```console
root@imx6ul-var-dart:~#mv rcstrackersimple.bak rcstrackersimple
```

## Analisi ricevitore GPS-GNSS ##
Per visualizzare in tempo reale le stringhe NMEA del ricevitore GPS, lanciare il comando:
```console
root@imx6ul-var-dart:~# Gps
```

Per chiudere il terminale GPS premere **CTRL+A e poi X** 


## Impostazioni Jumper Scheda ##
Nell'immagine sono evidenziati i jumper della scheda, con la loro funzionalità:

![jumpers](wiki/jumpers.png)

```console
+--------+--------+---------------------+-----------------+---------+
| Jumper |  Stato |       Funzione      |     Effetto     | Default |
+========+========+=====================+=================+=========+
|   JP1  | Aperto |  Power Management   |    Abilitato    |    *    |
+        +--------+                     +-----------------+---------+
|        | Chiuso |                     |  Disabilitato   |         |
+--------+--------+---------------------+-----------------+---------+
|   JP2  | Aperto |    Selezione Boot   |      Da SD      |         |
+--------+--------+                     +-----------------+---------+
|        | Chiuso |                     |     Da eMMC     |    *    |
+--------+--------+---------------------+-----------------+---------+
|   JP3  | Aperto | Rilevazione SD Card | SD Non Presente |         |
+--------+--------+                     +-----------------+---------+
|        | Chiuso |                     |   SD Presente   |    *    |
+--------+--------+---------------------+-----------------+---------+ 
```

Al termine della procedura di programmazione e collaudo, impostare i solder jumper della board nella configurazione di default, come mostrato nella tabella qui sopra.
